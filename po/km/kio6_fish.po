# translation of kio_fish.po to Khmer
# Khoem Sokhem <khoemsokhem@khmeros.info>, 2005, 2007, 2008, 2009.
# Eng Vannak <evannak@khmeros.info>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: kio_fish\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-30 00:37+0000\n"
"PO-Revision-Date: 2009-04-22 10:29+0700\n"
"Last-Translator: Khoem Sokhem <khoemsokhem@khmeros.info>\n"
"Language-Team: Khmer <support@khmeros.info>\n"
"Language: km\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: fish.cpp:321
#, kde-format
msgid "Connecting..."
msgstr "កំពុង​តភ្ជាប់..."

#: fish.cpp:647
#, kde-format
msgid "Initiating protocol..."
msgstr "កំពុង​ចាប់ផ្ដើម​ពិធីការ..."

#: fish.cpp:684
#, kde-format
msgid "Local Login"
msgstr "ការ​ចូល​មូលដ្ឋាន"

#: fish.cpp:686
#, fuzzy, kde-format
#| msgid "SSH Authorization"
msgid "SSH Authentication"
msgstr "ការ​អនុញ្ញាត​របស់ SSH"

#: fish.cpp:723 fish.cpp:738
#, kde-format
msgctxt "@action:button"
msgid "Yes"
msgstr ""

#: fish.cpp:723 fish.cpp:738
#, kde-format
msgctxt "@action:button"
msgid "No"
msgstr ""

#: fish.cpp:821
#, kde-format
msgid "Disconnected."
msgstr "បាន​ផ្ដាច់"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "ខឹម សុខែម,​អេង វណ្ណៈ, អោក ពិសិដ្ឋ​"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "khoemsokhem@khmeros.info,vannak@khmeros.info,piseth_dv@khmeros.info"
